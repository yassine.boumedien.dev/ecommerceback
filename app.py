from flask import Flask, render_template,request
from flask_restful import Resource, Api
import json
from flask_cors import CORS,cross_origin
import nltk
#stopwords = list of unnecessary words in any language
from nltk.corpus import stopwords
#stemmer= retourne la racine du mot (nicely=> nice) in lower case
from nltk.stem.snowball import FrenchStemmer
import numpy
from flask import Response,jsonify
from scipy import spatial


app = Flask(__name__)
api = Api(app)
CORS(app)

import mysql.connector

def getarticle():
        conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
        cursor=conn.cursor()
        cursor.execute("select * from produit")
        articles = cursor.fetchall()
        jsonResult = []
        for article in articles:
            jsonResult.append({
            'id':article[0],
            'nom':article[1],
            'couleur':article[2],
            'description':article[3],
            'prix':str(article[4]),
            'img':article[5]
        })
        conn.close()
        return jsonResult


@app.route('/articles',methods=['GET'])

def getall():
    response = getarticle()
    if len(response) == 0:
        return jsonify({"Error":"Sorry could not find any articles"})
    else:
        return json.dumps({"list":response})




def getid(id):
        conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
        cursor=conn.cursor()
        cursor.execute("select * from produit where idPdt="+id)
        articles = cursor.fetchall()
        jsonResult = []
        for article in articles:
            jsonResult.append({
            'id':article[0],
            'nom':article[1],
            'couleur':article[2],
            'description':article[3],
            'prix':str(article[4]),
            'img':article[5]

        })
        conn.close()
        return jsonResult[0]


@app.route('/article/<id>',methods=['GET'])

def getbyid(id):
    response = getid(id)
    if len(response) == 0:
        return jsonify({"Error":"Sorry could not find any articles"})
    else:
        return json.dumps({"article":response})





@app.route('/recommend/<id>',methods=['GET'])

def systemrecommend(id):
    def similariteCos(idpdtx,idpdty):
        return (1 - spatial.distance.cosine(matriceBin[idpdtx],matriceBin[idpdty]))
    
    conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
    cursor=conn.cursor()
    cursor.execute("select * from produit")

    #unnecessaryWords = set of unnecessasry words in french
    unnecessaryWords=set(stopwords.words("french"))
    #unnecessaryWords = transformation of the set to a list
    unnecessaryWords=list(unnecessaryWords)
    #extending the words of the list with the unnecessary ponctuations
    unnecessaryWords.extend([".",",","-",";","?","(",")",":"])



    #stemmer= retourne la racine du mot (nicely=> nice) in lower case in FRENCH
    stemmer = FrenchStemmer() 

    #Set of words that will contain all the words to make the binary matrix / vectors
    totalWords=set()

    #nombre de produits
    nbPdts=0

    #dictionnaire contenant l'id du produit comme indice et la description comme valeur
    dictPdtMots={}

    for ligne in cursor.fetchall():
        nbPdts+=1
        description=ligne[3]
        
        #tokenisation = separation des mots
        mots=nltk.word_tokenize(description)
        
        Mots=[m for m in mots if m.lower() not in unnecessaryWords]
        #print("\n",Mots,"\n")
        
        MotsStemmed=[]
        for m in Mots:
            MotsStemmed.append(stemmer.stem(m))
            #clean words in lower case
            #this can be deleted and replaced with only one loop of the totalwords to make vectors
    
        
        for m in MotsStemmed:
            totalWords.add(m)
        #ligne[0] = idproduit dans la base de données
        dictPdtMots[ligne[0]]=MotsStemmed


    conn.close()


    #nombre de mots total
    nbMots=len(totalWords)

    #matrice binaire d'occurence
    matriceBin=numpy.zeros((nbPdts,nbMots))

    #remplissage de la matrice binaire
    for i in range(nbPdts):
        k=0
        for j in totalWords:
            if j in dictPdtMots[i+1]:
                matriceBin[i][k]=1
            #print(matriceBin)
            k+=1
    
    #matrice de similarité produit x produits
    matriceSimilariteBin=numpy.zeros((nbPdts,nbPdts))

    #remplissage de la matrice de similarité
    for x in range(nbPdts):
        for y in range(nbPdts):
            matriceSimilariteBin[x][y]=similariteCos(x, y)

    s=int(id)-1
    sims=[]
    for k in range(3):
        maxi=0
        for z in range(nbPdts):
            if(matriceSimilariteBin[s][z]>maxi) and (matriceSimilariteBin[s][z]<1) and ((str(z+1)) not in sims):
                maxi=matriceSimilariteBin[s][z]
                idPdtMax=str(z+1)
        sims.append(idPdtMax)
    jsons=[];
    for r in range(len(sims)):
        jsons.append(getid(sims[r]))
    return json.dumps({"article":jsons})


@app.route('/login',methods=['POST'])
def login():
    data = request.form
    user=data.get("user")

    passs=data.get("passs")

    conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
    cursor=conn.cursor()
    cursor.execute("select * from user where user='"+user+"' and pass='"+passs+"'")
    article = cursor.fetchone()
    return json.dumps({"user":article[0],"name":article[1]})


@app.route('/note',methods=['POST'])
def note():
    data = request.form
    note=data.get("note")

    user=data.get("user")

    pdt=data.get("pdt")

    conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
    cursor=conn.cursor()
    cursor.execute("INSERT INTO notes VALUES('"+pdt+"', '"+user+"','"+note+"')")
    conn.commit()
    return json.dumps({"ex":"done"})









@app.route('/getnote/<idproduit>/<iduser>',methods=['GET'])

def getnote(idproduit,iduser):
    conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
    cursor=conn.cursor()
    cursor.execute("select * from notes where idPdt="+idproduit+" and IdUser="+iduser)
    articles = cursor.fetchall() 
    if len(articles) == 0:
        return jsonify({"Error":"Sorry could not find any articles"})
    else:
        return json.dumps({"note":str(articles[0][2])})



@app.route('/recommenduser/<idu>',methods=['GET'])

def systemrecommenduser(idu):
    def similariteUserCos(idUx,idUy):
        return (1 - spatial.distance.cosine(matriceNotes[idUx],matriceNotes[idUy]))
   
    conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
    cursor=conn.cursor()
    cursor.execute("select * from produit")
    nbPdts=0
    for ligne in cursor.fetchall():
        nbPdts+=1
    conn.close()
    conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
    cursor=conn.cursor()


    #récupération du nombre d'utilisateurs
    cursor.execute("select count(*) from user")
    #le curseur récupére le nombre de lignes et le met dans un tuple
    nbUserTuple=cursor.fetchone()
    #nombre de lignes
    nbUser=nbUserTuple[0]



    cursor.execute("select * from notes")
    #matrice user x produits => notes
    matriceNotes=numpy.zeros((nbUser,nbPdts))

    for notes in cursor.fetchall():

        #user
        i=(notes[1])-1
        #produit
        j=(notes[0])-1
        matriceNotes[i][j]=notes[2]


    #matrice similarité user x user
    matriceSimUser=numpy.zeros((nbUser,nbUser))

    #remplissage de la matrice de similarité
    for x in range(nbUser):
        for y in range(nbUser):
            matriceSimUser[x][y]=similariteUserCos(x, y)

    iduser=int(idu)
    max=0
    i=0
    for x in range(nbUser):
        if (matriceSimUser[iduser-1][x]>max and matriceSimUser[iduser-1][x]!=1):
            max=matriceSimUser[iduser-1][x]
            i=x
        
    pdts=[]
    for x in range(len(matriceNotes[iduser-1])):
        if (matriceNotes[iduser-1][x]==0 and matriceNotes[i][x]>4):
            pdts.append(x+1)  
    conn.close()
    jsons=[]
    for r in range(len(pdts)):
        jsons.append(getid(str(pdts[r])))
    conn.close()
    return json.dumps({"prod":jsons})







def gettop3():
        conn=mysql.connector.connect(host="localhost", port=3306,user="root",password="",database="sysrecv2")
        cursor=conn.cursor()
        cursor.execute("SELECT AVG(n.note),n.idPdt FROM `notes` n GROUP BY idPdt ORDER BY AVG(n.note) DESC")
        articles = cursor.fetchall()
        jsonResult = []
        for article in articles:
            jsonResult.append({
            'id':article[1],

        })
        conn.close()
        return jsonResult[0:3]


@app.route('/top3',methods=['GET'])

def top3():
    response = gettop3()
    res=[]
    for item in response:
        res.append(getid(str(item['id'])))
    if len(response) == 0:
        return jsonify({"Error":"Sorry could not find any articles"})
    else:
        return json.dumps({"list":res})




if __name__=="__main__":
    app.run(debug=True)